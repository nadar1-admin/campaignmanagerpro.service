#!/usr/bin/env zx

try {
    await $`echo NOTE: echo make sure you have zx binary installed... In case you dont: npm i -g zx`
    const version = await question("Specify new image tag: (example: 1.1)")

    await $`./push-image ${version}`
    
}
catch(processError) {
    console.error(processError.stderr);
}
