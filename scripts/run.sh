#!/bin/sh
stage="DEV"
if [[ $1 == "prod" ]]; then
    stage="PROD"
fi

echo activating env
cd ..
source ./env/Scripts/activate

cd src
ENV=$stage uvicorn main:app --reload