#!/bin/sh

local_repo=campaign-manager-backend:dev
remote_repo=912189574234.dkr.ecr.us-east-1.amazonaws.com/campaign-manager-backend
version=$1

if [[ $1 == "" ]]; then
    echo "ERROR: Please provide an argument specifying the image tag version"
    exit 1
fi

cd ..

aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 912189574234.dkr.ecr.us-east-1.amazonaws.com

docker build -t $local_repo -f Dockerfile-aws .

docker tag $local_repo $remote_repo:$version

docker push $remote_repo:$version

