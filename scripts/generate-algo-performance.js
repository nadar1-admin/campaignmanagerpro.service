const fs = require("fs");

const DAYS_TO_GENERATE = 50;
const dailyPerformanceSchema = {
    difficulty: 1,
    campaign_average_bid: 1.29,
    daily_performance: [
        {
            clicks: 220,
            applicants: 15,
            cost: 2400,
        },
    ],
};

const GenerateJsonFile = (filePath) => {
    const algoPerformance = {
        difficulty: dailyPerformanceSchema.difficulty,
        campaign_average_bid: dailyPerformanceSchema.campaign_average_bid,
        daily_performance: [],
    };
    const dailyPerformance = { ...dailyPerformanceSchema.daily_performance[0] };

    for (let index = 0; index < DAYS_TO_GENERATE; index++) {
        algoPerformance.daily_performance.push({ ...dailyPerformance });

        dailyPerformance.applicants += Math.round(
            dailyPerformanceSchema.daily_performance[0].applicants *
                Math.random()
        );
        dailyPerformance.clicks += Math.round(
            dailyPerformanceSchema.daily_performance[0].clicks * Math.random()
        );
        dailyPerformance.cost += Math.round(
            dailyPerformanceSchema.daily_performance[0].cost * Math.random()
        );
    }

    fs.writeFileSync(filePath, JSON.stringify(algoPerformance, "utf8"));
};

GenerateJsonFile(
    "C:/Test-Projects/Hachaton-1-Tests/SQLite3-FastAPI-RND/src/data/algo_performance.json"
);
