import json
import os
from typing import Dict


class GameSetupRepository:
    fileDir = os.path.dirname(__file__)
    file_name = "../game_setups.json"

    path = os.path.join(fileDir, file_name)

    def get_setup(self, difficulty: int = None):

        with open(self.path) as f:
            json_data = json.load(f)

            if difficulty is None:
                return json_data

            if difficulty < 1 or difficulty > 3:
                raise Exception("Difficulty must be between 1 to 3")

            return json_data[difficulty - 1]


game_setup_repository = GameSetupRepository()
