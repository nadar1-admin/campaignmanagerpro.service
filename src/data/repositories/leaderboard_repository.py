from datetime import datetime
import os
import json
from typing import Any, Dict, List, Optional, Union
from sqlalchemy.orm import Session
from sqlalchemy.sql.expression import extract
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.sqltypes import DateTime
from data.repositories.generic_repository import GenericRepository
from models.leaderboard_score import LeaderboardScore
from services.leaderboards.dto.create_leaderboard_score_request import CreateLeaderboardScoreRequest
from services.performance.dto.get_performance_request import GetPerformanceRequest
from services.performance.dto.save_job_vendor_performance_request import SaveJobVendorPerformanceRequest
from services.leaderboards.dto.get_leaderboards_rank_response import GetLeaderboardRankResponse


class LeaderboardRepository(GenericRepository[LeaderboardScore, CreateLeaderboardScoreRequest]):
    def get_leaderboard_rank(self, leaderboard_score_id: int, db: Session) -> GetLeaderboardRankResponse:

        # public.get_leaderboards_rank() is the PostgresSQL function
        query_response = db \
            .query(func.public.get_leaderboards_rank_monthly(leaderboard_score_id)) \
            .first()

        response_as_tuple = eval(query_response[0])  # Sorry god..
        return GetLeaderboardRankResponse(**{
            "Id": response_as_tuple[0],
            "rankPlace": response_as_tuple[1],
            "score": response_as_tuple[2],
            "createdate": response_as_tuple[3],
        })

    def get_monthly_leaderboards(self, db: Session) -> List[LeaderboardScore]:
        return db \
            .query(self.model) \
            .filter(extract('year', self.model.CreateDate) == datetime.utcnow().year,
                    extract('month', self.model.CreateDate) == datetime.utcnow().month) \
            .order_by(self.model.Score.desc()) \
            .limit(10) \
            .all()


leaderboard_repository = LeaderboardRepository(LeaderboardScore)
