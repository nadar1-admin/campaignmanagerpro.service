import os
import json
from services.performance.dto.get_performance_request import GetPerformanceRequest
from services.performance.dto.save_job_vendor_performance_request import SaveJobVendorPerformanceRequest
from models.job_vendor_performance import JobVendorPerformance
from data.repositories.generic_repository import GenericRepository
from typing import Any, Dict, List, Optional, Union

from sqlalchemy.orm import Session


class PerformanceRepository(GenericRepository[JobVendorPerformance, SaveJobVendorPerformanceRequest]):

    def get(self, db: Session, *, get_request: GetPerformanceRequest) -> JobVendorPerformance:
        return db \
            .query(self.model) \
            .filter(self.model.GameId == get_request.GameId, self.model.JobId == get_request.JobId, self.model.VendorId == get_request.VendorId) \
            .order_by(self.model.CreateDate.desc()) \
            .first()

    def get_batch(self, db: Session, *, get_batch_request: List[GetPerformanceRequest]) -> List[JobVendorPerformance]:
        return [self.get(db, get_request=get_request) for get_request in get_batch_request]

    def create_batch(self, db: Session, *, create_batch_request: List[SaveJobVendorPerformanceRequest]) -> List[JobVendorPerformance]:
        # db.bulk_save_objects()
        return [self.create(db, obj_in=create_request) for create_request in create_batch_request]

    def get_algo_performance(self, day):
        FILE_NAME = "algo_performance.json"
        file_dir = os.path.dirname(__file__)
        file_path = os.path.join(file_dir, "..", FILE_NAME)
        with open(file_path) as f:
            algo_performance = json.load(f)

            # Days are indexed from 1, so we remove 1 for 0 based index
            return algo_performance["daily_performance"][day - 1]

    # def get_multi(self, db: Session, *, get_batch_request: List[GetPerformanceRequest]) -> List[JobVendorPerformance]:
    #     return db \
    #         .query(self.model) \
    #         .filter(self.model.job_id == get_batch_request.job_id, self.model.vendor_id == get_batch_request.vendor_id) \
    #         .order_by(self.model.create_date.desc()) \
    #         .all()


performance_repository = PerformanceRepository(JobVendorPerformance)
