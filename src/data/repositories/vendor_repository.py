import json
import os
from typing import Dict


class VendorRepository:
    fileDir = os.path.dirname(__file__)
    path = os.path.join(fileDir, "../vendors.json")

    def get_by_id(self, vendor_id: int) -> str:
        with open(self.path) as f:
            vendors = json.load(f)
            for vendor in vendors:
                if vendor['vendor_id'] == vendor_id:
                    return vendor['vendor_name']

            raise Exception(f"There is no vendor with id: {vendor_id}")

    def get_all(self) -> str:
        with open(self.path) as f:
            vendors = json.load(f)
            return vendors


vendor_repostiry = VendorRepository()
