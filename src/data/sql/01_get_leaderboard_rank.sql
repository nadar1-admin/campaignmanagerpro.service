-- Function script
create or replace function get_leaderboards_rank(leaderboard_id int) 
returns table (id int, rankPlace int, score decimal, createdate timestamp)
LANGUAGE sql
AS $$
   with ranked_table as
	(
		--return query
		select
			*,
			row_number() over(order by t."Score" desc) as rankplace
		from public.leaderboardscore t
	)
	select
	t."Id"
	,t."rankplace"
	,t."Score"
	,t."CreateDate"
	from ranked_table t
	where t."Id" = leaderboard_id;
$$;


-- Monthly function
create or replace function get_leaderboards_rank_monthly(leaderboard_id int) 
returns table (id int, rankPlace int, score decimal, createdate timestamp)
LANGUAGE sql
AS $$
   with ranked_table as
	(
		select
			*,
			row_number() over(order by t."Score" desc) as rankplace
		from public.leaderboardscore t
		where 
			extract(year from t."CreateDate") = extract(year from current_timestamp)
			and
			extract(month from t."CreateDate") = extract(month from current_timestamp)
	)
	select
	t."Id"
	,t."rankplace"
	,t."Score"
	,t."CreateDate"
	from ranked_table t
	where t."Id" = leaderboard_id;
$$;