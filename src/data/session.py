from datetime import datetime

from sqlalchemy.sql.schema import ForeignKey
from config.db_tables import db_tables
import sqlalchemy as db
from config.settings import settings
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


engine = create_engine(settings.CONNECTION_STRING, pool_pre_ping=True)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

# Create tables code first (only on first time)
metadata = db.MetaData()
job_vendors_performance = db.Table(
    db_tables.job_vendor_performance,
    metadata,
    db.Column("Id", db.INTEGER, primary_key=True, autoincrement=True),
    db.Column("GameId", db.VARCHAR),
    db.Column("UserName", db.VARCHAR),
    db.Column("Age", db.INTEGER),
    db.Column("CreateDate", db.TIMESTAMP, default=datetime.utcnow),
    db.Column("JobId", db.INTEGER, nullable=False),
    db.Column("VendorId", db.INTEGER, nullable=False),
    db.Column("Bid", db.DECIMAL, nullable=False),
    db.Column("Difficulty", db.INTEGER, nullable=False),
    db.Column("Clicks", db.DECIMAL, nullable=False),
    db.Column("Applicants", db.DECIMAL, nullable=False),
    db.Column("Cost", db.DECIMAL, nullable=False),
    db.Column("CPA", db.DECIMAL, nullable=False),
    db.Column("CVR", db.DECIMAL, nullable=False),
)

leaderboard_scores_table = db.Table(
    db_tables.leaderboard_score,
    metadata,
    db.Column("Id", db.INTEGER, primary_key=True, autoincrement=True),
    db.Column("GameId", db.VARCHAR),
    db.Column("GameLevel", db.INTEGER),
    db.Column("UserName", db.VARCHAR),
    db.Column("Score", db.DECIMAL, nullable=False),
    db.Column("CreateDate", db.TIMESTAMP, default=datetime.utcnow),
    db.Column("HasReachedGoal", db.BOOLEAN, nullable=False),
    db.Column("HasBeatenAlgo", db.BOOLEAN, nullable=False),
)

metadata.create_all(engine)
