from os import environ
from fastapi import FastAPI
import controllers.game_controller as game_controller
import controllers.leaderboards_controller as leaderboards_controller
import controllers.slack_controller as slack_controller
from fastapi.middleware.cors import CORSMiddleware

origins = [
    "http://localhost",
    "http://localhost:3000",
    "http://localhost:3001",
    "http://localhost:8000",
    "http://campaign-manager-web-prod.s3-website-us-east-1.amazonaws.com",
    "https://cmanager-pro.algoteam.net",
]

print("Running in environment:", environ.get("ENV"))

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(
    game_controller.router,
    prefix="/game",
    tags=['Game']
)


app.include_router(
    leaderboards_controller.router,
    prefix="/leaderboards",
    tags=['Leaderboards']
)

app.include_router(
    slack_controller.router,
    prefix="/slack",
    tags=['Slack']
)
