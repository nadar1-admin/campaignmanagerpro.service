from datetime import datetime

from sqlalchemy.sql.schema import ForeignKey
from models.base_class import BaseClass
from sqlalchemy import Column
from sqlalchemy.sql.sqltypes import INTEGER, VARCHAR, DECIMAL, TIMESTAMP, BOOLEAN


class LeaderboardScore(BaseClass):
    Id = Column(INTEGER, primary_key=True, autoincrement=True)
    GameId = Column(VARCHAR, nullable=False)
    GameLevel = Column(INTEGER, nullable=False)
    UserName = Column(VARCHAR, nullable=False)
    Score = Column(DECIMAL, nullable=False)
    CreateDate = Column(TIMESTAMP, nullable=False)
    HasReachedGoal = Column(BOOLEAN, nullable=False)
    HasBeatenAlgo = Column(BOOLEAN, nullable=False)
