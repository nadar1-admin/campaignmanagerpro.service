from datetime import datetime
from models.base_class import BaseClass
from sqlalchemy import Column
from sqlalchemy.sql.sqltypes import INTEGER, VARCHAR, DECIMAL, TIMESTAMP


class JobVendorPerformance(BaseClass):
    Id = Column(INTEGER, primary_key=True, autoincrement=True)
    GameId = Column(VARCHAR, nullable=False)
    UserName = Column(VARCHAR, nullable=False)
    Age = Column(INTEGER, nullable=False)
    CreateDate = Column(TIMESTAMP, default=datetime.utcnow)
    JobId = Column(INTEGER, nullable=False)
    VendorId = Column(INTEGER, nullable=False)
    Bid = Column(DECIMAL, nullable=False)
    Difficulty = Column(INTEGER, nullable=False)
    Clicks = Column(DECIMAL, nullable=False)
    Applicants = Column(DECIMAL, nullable=False)
    Cost = Column(DECIMAL, nullable=False)
    CPA = Column(DECIMAL, nullable=False)
    CVR = Column(DECIMAL, nullable=False)
