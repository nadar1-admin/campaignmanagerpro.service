from sqlalchemy.orm.session import Session
from errors.entity_not_found_exception import EntityNotFoundException
from services.performance.dto.save_job_vendor_performance_request import SaveJobVendorPerformanceRequest
from models.job_vendor_performance import JobVendorPerformance
from typing import List
from config.db_tables import db_tables
from services.performance.dto.get_performance_request import GetPerformanceRequest
from services.game.dto.user_bid_request import UserBidRequest
from data.repositories.performance_repository import performance_repository


def get_performance(get_performance_request: GetPerformanceRequest, user_bid_request: UserBidRequest,  db: Session) -> JobVendorPerformance:
    jobvendor_performance = performance_repository.get(
        db,
        get_request=get_performance_request
    )

    if jobvendor_performance is None:
        initial_state = JobVendorPerformance(
            GameId=user_bid_request.GameId,
            JobId=user_bid_request.JobId,
            VendorId=user_bid_request.VendorId,
            Bid=user_bid_request.Bid,
            Difficulty=0,
            Clicks=0,
            Applicants=0,
            Cost=0,
            CPA=0,
            CVR=0
        )
        return initial_state

    return jobvendor_performance


def get_performance_batch(get_batch_requset: List[GetPerformanceRequest], db: Session) -> List[JobVendorPerformance]:
    return [get_performance(get_performance_request, db) for get_performance_request in get_batch_requset]


def save_performance(save_job_vendor_performance_request: SaveJobVendorPerformanceRequest, db: Session) -> JobVendorPerformance:
    return performance_repository.create(
        db,
        obj_in=save_job_vendor_performance_request
    )


def save_performance_batch(save_job_vendor_performance_batch_request: List[SaveJobVendorPerformanceRequest], db: Session) -> List[JobVendorPerformance]:
    return performance_repository.create_batch(
        db,
        create_batch_request=save_job_vendor_performance_batch_request
    )
