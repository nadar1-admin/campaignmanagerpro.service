from datetime import datetime
from typing import Any, Dict
from pydantic import BaseModel


class SaveJobVendorPerformanceRequest(BaseModel):
    GameId: str
    UserName: str
    JobId: int
    VendorId: int
    Bid: float
    Difficulty: str
    Age: int
    Clicks: int
    Applicants: int
    Cost: int
    CPA: float
    CVR: float
