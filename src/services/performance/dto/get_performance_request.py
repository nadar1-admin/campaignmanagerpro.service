from pydantic import BaseModel


class GetPerformanceRequest(BaseModel):
    GameId: str
    JobId: int
    VendorId: int
