from pydantic import BaseModel

"""
The requests that comes from the user
when he wants to place a bid for job/vendor
"""


class UserBidRequest(BaseModel):
    GameId: str
    UserName: str
    JobId: int
    VendorId: float
    Difficulty: int
    Bid: float
    Age: int
