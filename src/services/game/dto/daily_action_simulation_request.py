from services.game.dto.user_bid_request import UserBidRequest
from models.job_vendor_performance import JobVendorPerformance
from pydantic import BaseModel
from datetime import datetime


class DailyActionSimulationRequest(BaseModel):
    JobId: int
    VendorId: int
    CurrentBid: float
    NewBid: float
    Difficulty: int
    ActiveDate: datetime
    Age: int  # denotes the active days for the campai
    Clicks: int
    Applicants: int
    Cost: int

    # HardCode for now
    # TODO: REMOVE HARD CODE
    CampaignBudget: int = 25000

    @staticmethod
    def create(jobvendor_performance: JobVendorPerformance, user_bid_request: UserBidRequest):
        return DailyActionSimulationRequest(
            JobId=jobvendor_performance.JobId,
            VendorId=jobvendor_performance.VendorId,
            CurrentBid=jobvendor_performance.Bid,
            NewBid=user_bid_request.Bid,
            Difficulty=jobvendor_performance.Difficulty,
            ActiveDate=datetime.utcnow(),  # TODO: refactor
            Age=1,  # TODO: refactor
            Clicks=jobvendor_performance.Clicks,
            Applicants=jobvendor_performance.Applicants,
            Cost=jobvendor_performance.Cost,
        )
