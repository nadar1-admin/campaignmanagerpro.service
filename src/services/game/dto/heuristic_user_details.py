from pydantic import BaseModel


class HeuristicUserDetails(BaseModel):
    TotalCost: float
    Bid: float
    ExportPercentage: float
    Budget: float
