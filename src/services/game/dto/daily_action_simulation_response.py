from services.game.dto.user_bid_request import UserBidRequest
from models.job_vendor_performance import JobVendorPerformance
from pydantic import BaseModel
from datetime import datetime


class DailyActionSimulationResponse(BaseModel):
    JobId: int
    VendorId: int
    Bid: float
    Difficulty: str
    ActiveDate: datetime
    Age: int
    Clicks: int
    Applicants: int
    Cost: int
