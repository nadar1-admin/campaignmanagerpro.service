from datetime import datetime
from typing import List
from pydantic import BaseModel


class JobPerformanceForVendor(BaseModel):
    job_id: int
    vendor_id: int
    vendor_name: str
    current_bid: float
    clicks: int
    applicants: int
    cost: int
    cvr: float
    cpa: float


class JobPerformanceByVendors(BaseModel):
    job_id: int
    current_bid: float
    applicants: int
    clicks: int
    cost: int
    cvr: float
    cpa: float
    vendor_specifics: List[JobPerformanceForVendor]
