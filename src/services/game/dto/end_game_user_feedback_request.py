from pydantic import BaseModel
from datetime import datetime


class EndGameUserFeedbackRequest(BaseModel):
    GameId: str
