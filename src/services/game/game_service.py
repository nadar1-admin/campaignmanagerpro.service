

from typing import Dict, List
import requests
import json
from fastapi.encoders import jsonable_encoder

from sqlalchemy.orm.session import Session
from config.settings import settings
from data.repositories.performance_repository import performance_repository
from data.repositories.vendor_repository import vendor_repostiry
from data.repositories.game_setup_repository import game_setup_repository
from models.job_vendor_performance import JobVendorPerformance
from services.game.dto.daily_action_simulation_request import DailyActionSimulationRequest
from services.game.dto.daily_action_simulation_response import DailyActionSimulationResponse
from services.performance.dto.get_performance_request import GetPerformanceRequest
from services.game.dto.job_performance_by_vendors import JobPerformanceByVendors
from services.game.dto.user_bid_request import UserBidRequest
from services.game.dto.job_performance_by_vendors import JobPerformanceForVendor
from services.performance.dto.save_job_vendor_performance_request import SaveJobVendorPerformanceRequest
from services.game.dto.end_game_user_feedback_request import EndGameUserFeedbackRequest
from services.game.dto.heuristic_user_details import HeuristicUserDetails
import services.performance.performance_service as performance_service


class GameService():
    def __init__(self) -> None:
        pass

    def get_game_setup(self, difficulty: int):
        return game_setup_repository.get_setup(difficulty)

    def get_game_setups_list(self):
        return game_setup_repository.get_setup()

    def get_vendors(self):
        return vendor_repostiry.get_all()

    def _create_simulation_request(self, user_bids_request: List[UserBidRequest], db: Session) -> List[DailyActionSimulationRequest]:
        performance_request_list = [GetPerformanceRequest(
            GameId=bid_request.GameId,
            JobId=bid_request.JobId,
            VendorId=bid_request.VendorId,
        ) for bid_request in user_bids_request]

        jobvendor_performance_list = [performance_service.get_performance(performance_request, user_bids_request[index],  db)
                                      for index, performance_request in enumerate(performance_request_list)]

        simulation_request_list = [DailyActionSimulationRequest.create(jobvendor_performance, user_bid_request) for (
            jobvendor_performance, user_bid_request) in zip(jobvendor_performance_list, user_bids_request)]

        return simulation_request_list

    def _send_to_ml_api(self, action_simulation_request: List[DailyActionSimulationRequest]) -> List[DailyActionSimulationResponse]:

        data = jsonable_encoder(action_simulation_request)

        headers = {
            'Cnvrg-Api-Key': settings.CNVRG_API_KEY,
            'Content-Type': "application/json"
        }

        result = requests.post(
            url=settings.ML_ENDPOINT,
            headers=headers,
            data=json.dumps({"input_params": data})
        )

        json_response = result.json()["prediction"]
        simulation_response: List[DailyActionSimulationResponse] = [
            DailyActionSimulationResponse(**res) for res in json_response
        ]

        # Aggregate request into simulation response since ML API doesn't return aggregated
        for i, _ in enumerate(simulation_response):
            simulation_response[i].Applicants += action_simulation_request[i].Applicants
            simulation_response[i].Cost += action_simulation_request[i].Cost
            simulation_response[i].Clicks += action_simulation_request[i].Clicks

        return simulation_response

    def _save_simulation_response_to_db(self, sample_user_bid_request: UserBidRequest, simulation_response: List[DailyActionSimulationResponse], db: Session) -> None:
        save_jobvendor_performance_request_list = [SaveJobVendorPerformanceRequest(
            GameId=sample_user_bid_request.GameId,
            UserName=sample_user_bid_request.UserName,
            Age=sample_user_bid_request.Age,
            JobId=action_simulation_response.JobId,
            VendorId=action_simulation_response.VendorId,
            Bid=action_simulation_response.Bid,
            Difficulty=action_simulation_response.Difficulty,
            Clicks=action_simulation_response.Clicks,
            Applicants=action_simulation_response.Applicants,
            Cost=action_simulation_response.Cost,
            CPA=0 if action_simulation_response.Applicants == 0 else action_simulation_response.Cost /
            action_simulation_response.Applicants,
            CVR=0 if action_simulation_response.Clicks == 0 else action_simulation_response.Applicants /
            action_simulation_response.Clicks
        ) for action_simulation_response in simulation_response]

        performance_repository.create_batch(
            db, create_batch_request=save_jobvendor_performance_request_list)

    def _aggregate_performance_by_vendors(self, simulation_result: List[DailyActionSimulationResponse]) -> List[JobPerformanceByVendors]:
        simulation_aggregated_response: List[JobPerformanceByVendors] = []
        index_caching: Dict[str, int] = {}

        for res in simulation_result:
            if res.JobId not in index_caching:
                simulation_aggregated_response.append(JobPerformanceByVendors(
                    job_id=res.JobId,
                    current_bid=res.Bid,
                    applicants=0,
                    clicks=0,
                    cost=0,
                    cvr=0,
                    cpa=0,
                    vendor_specifics=[]
                ))

                index_caching[res.JobId] = len(
                    simulation_aggregated_response) - 1

            jobperformance_by_vendor = simulation_aggregated_response[index_caching[res.JobId]]
            jobperformance_by_vendor.clicks += res.Clicks
            jobperformance_by_vendor.applicants += res.Applicants
            jobperformance_by_vendor.cost += res.Cost
            jobperformance_by_vendor.cvr = 0 if jobperformance_by_vendor.clicks == 0 else jobperformance_by_vendor.applicants / \
                jobperformance_by_vendor.clicks
            jobperformance_by_vendor.cpa = 0 if jobperformance_by_vendor.applicants == 0 else jobperformance_by_vendor.cost / \
                jobperformance_by_vendor.applicants

            jobperformance_by_vendor.vendor_specifics.append(JobPerformanceForVendor(
                job_id=res.JobId,
                vendor_id=res.VendorId,
                vendor_name=vendor_repostiry.get_by_id(res.VendorId),
                current_bid=res.Bid,
                clicks=res.Clicks,
                applicants=res.Applicants,
                cost=res.Cost,
                cvr=0 if res.Clicks == 0 else res.Applicants / res.Clicks,
                cpa=0 if res.Applicants == 0 else res.Cost / res.Applicants
            ))

        return simulation_aggregated_response

    def simulate_day(self, user_bids_request: List[UserBidRequest], db: Session) -> List[JobPerformanceByVendors]:
        simulation_request_list = self._create_simulation_request(
            user_bids_request, db)

        daily_simulation_response = self._send_to_ml_api(
            simulation_request_list)

        self._save_simulation_response_to_db(
            user_bids_request[0], daily_simulation_response, db)

        return self._aggregate_performance_by_vendors(daily_simulation_response)

    def get_algo_daily_performance(self, day):
        return performance_repository.get_algo_performance(day)

    def _get_heuristic_user_details(self):
        return HeuristicUserDetails(
            Budget=5000,
            TotalCost=4000,
            Bid=1.05,
            ExportPercentage=0.12
        )

    def generate_end_game_user_feedback(self, feedback_request: EndGameUserFeedbackRequest) -> List[str]:
        user_details = self._get_heuristic_user_details()

        return ["Diversify your jobs", "Lower your bids", "Eat breakfast"]


game_service = GameService()
