import json
import requests as r
from services.slack.dto.slack_message import SlackMessage
from services.slack.dto.user_won_notification_request import UserWonNotificationRequest
from config.settings import settings


class SlackService():
    POST_MESSAGE_URL = "https://slack.com/api/chat.postMessage"

    def write_to_slack_channel(self, message: SlackMessage):
        try:
            res = r.post(self.POST_MESSAGE_URL, data={
                "token": settings.BOT_TOKEN,
                "channel": settings.SLACK_NOTIFICATION_CHANNEL,
                "text": message
            })
        except Exception as e:
            print(e)
            raise e

    def celebrate_user_won(self, user_won_notification: UserWonNotificationRequest):
        """
        When user beas the algo
        """
        try:
            res = r.post(self.POST_MESSAGE_URL, data={
                "token": settings.BOT_TOKEN,
                "channel": settings.SLACK_NOTIFICATION_CHANNEL,
                "text": f"""
                Ladies and gentleman!\n*{user_won_notification.UserName}* has won algo in Campaign Manager Pro with a score of: *{user_won_notification.Score}*\n{user_won_notification.UserName}'s place is now #*{user_won_notification.Rank}*
                """
            })
            print(res)
        except Exception as e:
            print(e)
