
from pydantic import BaseModel


class UserWonNotificationRequest(BaseModel):
    UserName: str
    Score: float
    Rank: int
