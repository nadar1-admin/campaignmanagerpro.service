from pydantic import BaseModel


class SlackMessage(BaseModel):
    Message: str
