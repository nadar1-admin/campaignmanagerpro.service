from models.leaderboard_score import LeaderboardScore
from services.leaderboards.dto.get_leaderboards_rank_response import GetLeaderboardRankResponse


class CreateLeaderboardScoreResponse():
    EntityCreated: LeaderboardScore
    Rank: GetLeaderboardRankResponse

    def __init__(self, entityCreated, rank) -> None:
        self.EntityCreated = entityCreated
        self.Rank = rank
