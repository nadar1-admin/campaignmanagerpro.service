from typing import Optional
from pydantic import BaseModel
from datetime import datetime


class CreateLeaderboardScoreRequest(BaseModel):
    GameId: str
    GameLevel: int
    UserName: str
    Score: float
    HasReachedGoal: bool
    HasBeatenAlgo: bool
    CreateDate: Optional[datetime]
