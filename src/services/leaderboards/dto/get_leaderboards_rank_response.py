from typing import Optional
from pydantic import BaseModel
from datetime import datetime


class GetLeaderboardRankResponse(BaseModel):
    """
    Class that represents the response from get_leaderboards_rank DB function
    """
    Id: int
    rankPlace: int
    score: float
    createdate: datetime
