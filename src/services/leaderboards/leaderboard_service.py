
from datetime import datetime
from typing import List, Tuple
from sqlalchemy.orm.session import Session
from data.repositories.leaderboard_repository import leaderboard_repository
from models.leaderboard_score import LeaderboardScore
from services.leaderboards.dto.create_leaderboard_score_request import CreateLeaderboardScoreRequest
from services.leaderboards.dto.get_leaderboards_rank_response import GetLeaderboardRankResponse
from services.leaderboards.dto.create_leaderboard_score_response import CreateLeaderboardScoreResponse


class LeaderboardService():

    def get_score_rank(self, leaderboard_score_id: int, db: Session) -> GetLeaderboardRankResponse:
        """
        Gets the score rank in this month
        """
        return leaderboard_repository.get_leaderboard_rank(leaderboard_score_id, db)

    def get_monthly_leaderboards(self, db: Session) -> List[LeaderboardScore]:
        """
        Retrieves the leaderboards for the current month
        """
        return leaderboard_repository.get_monthly_leaderboards(db)

    def save_leaderboard_score(self, leaderboard_score: CreateLeaderboardScoreRequest, db: Session) -> CreateLeaderboardScoreResponse:
        """
        Saves a leaderboard score in the database
        """
        if leaderboard_score.CreateDate is None:
            leaderboard_score.CreateDate = datetime.utcnow()

        created_entity = leaderboard_repository.create(
            db, obj_in=leaderboard_score)

        rank_response = self.get_score_rank(created_entity.Id, db)
        return CreateLeaderboardScoreResponse(
            created_entity,
            rank_response
        )


leaderboard_service = LeaderboardService()
