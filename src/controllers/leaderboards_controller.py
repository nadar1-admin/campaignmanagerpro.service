from api.deps import get_db
from fastapi import APIRouter
from typing import List, Dict
from fastapi.param_functions import Depends
from fastapi import HTTPException
from sqlalchemy.orm.session import Session
from services.leaderboards.dto.create_leaderboard_score_request import CreateLeaderboardScoreRequest
from services.leaderboards.leaderboard_service import leaderboard_service

router = APIRouter()


@router.get("")
def get_monthly_leaderboards(
    db: Session = Depends(get_db),
):
    """
    Returns the leaderboards for the current month
    """
    return leaderboard_service.get_monthly_leaderboards(db)


@router.get("/{leaderboard_score_id}")
def get_leaderboard_score_rank(
    leaderboard_score_id: int,
    db: Session = Depends(get_db),
):
    """
    Returns the rank of the given leaderboard id in this month
    """
    return leaderboard_service.get_score_rank(leaderboard_score_id, db)


@router.post("")
def save_leaderboard_score(
    create_leaderboard_request: CreateLeaderboardScoreRequest,
    db: Session = Depends(get_db),
):
    return leaderboard_service.save_leaderboard_score(
        create_leaderboard_request, db)
