from api.deps import get_db
from fastapi import APIRouter
from typing import List, Dict
from fastapi.param_functions import Depends
from fastapi import HTTPException
from sqlalchemy.orm.session import Session
from services.game.dto.end_game_user_feedback_request import EndGameUserFeedbackRequest
from services.game.game_service import game_service
from services.game.dto.user_bid_request import UserBidRequest

router = APIRouter()


@router.get("/game_setup")
def get_game_setup(
    db: Session = Depends(get_db),
):
    try:
        return game_service.get_game_setups_list()
    except:
        pass


@router.get("/game_setup/{difficulty}")
def get_game_setup(
    difficulty: int,
    db: Session = Depends(get_db),
):
    try:
        return game_service.get_game_setup(difficulty)
    except:
        raise HTTPException(
            status_code=400, detail="Difficulty must be between 1 to 3")


@router.get("/vendors")
def get_vendors(
    #db: Session = Depends(get_db),
):
    # try:
    return game_service.get_vendors()
    # except:
    #     raise HTTPException(
    #         status_code=400, detail="Something went wrong")


@router.post("/simulate_day")
def simulate_day(
    user_bids_request: List[UserBidRequest],
    db: Session = Depends(get_db),
):
    user_daily_performance = game_service.simulate_day(user_bids_request, db)
    algo_daily_performance = game_service.get_algo_daily_performance(
        user_bids_request[0].Age)

    return {
        "user_performance": user_daily_performance,
        "algo_performance": algo_daily_performance
    }


@router.post("/user_feedback_heuristic")
def user_feedback_heuristic(
    feedback_request: EndGameUserFeedbackRequest,
    db: Session = Depends(get_db),
):
    return game_service.generate_end_game_user_feedback(feedback_request)


@router.get("/leaderboards")
def get_leaderboards():
    pass
