from api.deps import get_db
from fastapi import APIRouter
from typing import List, Dict
from fastapi.param_functions import Depends
from fastapi import HTTPException
from sqlalchemy.orm.session import Session
from services.slack.slack_service import SlackService
from services.slack.dto.slack_message import SlackMessage
from services.slack.dto.user_won_notification_request import UserWonNotificationRequest

router = APIRouter()
slack_service = SlackService()


@router.post("/write-message")
def write_slack_message(
    slackMessage: SlackMessage,
    db: Session = Depends(get_db),
):
    slack_service.write_to_slack_channel(slackMessage)


@router.post("/celebrate-user-won")
def write_slack_message(
    user_won_notification: UserWonNotificationRequest,
    db: Session = Depends(get_db),
):
    slack_service.celebrate_user_won(user_won_notification)
