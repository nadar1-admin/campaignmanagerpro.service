class DbTables():
    job_vendor_performance: str = "jobvendorperformance"
    leaderboard_score: str = "leaderboardscore"


class DbFunctions():
    get_leaderboards_rank: str = "get_leaderboards_rank"


db_tables = DbTables()
db_functions = DbFunctions()
