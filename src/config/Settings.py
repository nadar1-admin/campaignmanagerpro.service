import os


class BaseSettings():
    BOT_TOKEN = "xoxb-442510665348-2469337009351-RVQYhJmjrzYlY2mKy0FaRF1J"
    ML_ENDPOINT: str = 'https://cmp-production-2.pandologic.cnvrg.io/api/v1/endpoints/cvrxrsrx7spmwtbrcphm'
    CNVRG_API_KEY: str = 'NPHQowZN9GKyg1fXeahH'


class TestSettings(BaseSettings):
    SLACK_NOTIFICATION_CHANNEL = "C02BS77C72T"  # Godly-testing channel
    CONNECTION_STRING: str = "postgresql://postgres:admin@127.0.0.1:5432/campaign-manager-pro"


class ProdSettings(BaseSettings):
    SLACK_NOTIFICATION_CHANNEL = "C02BS77C72T"
    # SLACK_NOTIFICATION_CHANNEL = "CUU6HQE74"  # THIS IS il-office
    CONNECTION_STRING: str = "postgresql://postgres:campaignmanagerpro123!@campaign-manager-pro.cdbwpbdmrm83.us-east-1.rds.amazonaws.com:5432/campaign-manager-pro"


def get_settings():
    if os.environ.get("ENV") == "PROD":
        return ProdSettings()

    return TestSettings()


settings = get_settings()
