# Campaign Manager 2021 - Backend

## Test Request Payload

    [
      {
        "JobId": 1,
        "VendorId": 8256,
        "Difficulty": 1,
        "Bid": 1
      },
      {
        "JobId": 2,
        "VendorId": 8256,
        "Difficulty": 2,
        "Bid": 2
      }
    ]

## Schemas

    [From front-end to server. A batch of these represent a simulation request]
    <<UserBidRequest>>
    {
    	JobId: <int>,
    	VendorId: <int>,
    	NewBid: <float>,
    	Difficulty: <string>,
    	
    }


    [ML Requets prediction API Contract]
    <<DailyActionsSimulationRequest>>
    {
    	JobId: <int>,
    	VendorId: <int>,
    	CurrentBid: <float>,
    	NewBid: <float>,
    	Difficulty: <string>,
    	ActiveDate: <datetime>,
    	Age: <int>, // denotes the active days for the campaign
    	Clicks: <int>,
    	Applicants: <int>,
    	Cost: <int>,
    }



    [ML Response prediction API Contract]
    <<DailyActionsSimulationResponse>>
    {
    	JobId: <int>,
    	VendorId: <int>,
    	Bid: <float>,
    	Difficulty: <string>,
    	ActiveDate: <datetime>,
    	Age: <int>, // denotes the active days for the campaign
    	Clicks: <int>,
    	Applicants: <int>,
    	Cost: <int>,
    }
